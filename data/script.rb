require "yaml"

d = Marshal.load(File.binread("./phone_data.orig.dat"))

ab = {
  :id=>"AB",
  :country_code=>"7",
  :international_prefix => "810",
  :types => {
    :general_desc => {
      :national_number_pattern => "(8402\\d{6}|940\\d{7})",
      :possible_number_pattern=>"\\d{10}"},
    :fixed_line=>{:national_number_pattern=>"8402[2-8]\\d{5}", :possible_number_pattern=>"\\d{10}"},
    :mobile=>{:national_number_pattern=>"940\\d{7}", :possible_number_pattern=>"\\d{10}"}}}

d["AB"] = ab

ru = d["RU"]
ru_f = ru[:types][:fixed_line]
ru_f[:national_number_pattern] = "((?:3(?:0[12]|4[1-35-79]|5[1-3]|8[1-58]|9[0145])|4(?:01|1[1356]|2[13467]|7[1-5]|8[1-7]|9[1-689])|8(?:1[1-8]|2[01]|3[13-6]|4[1-8]|5[15]|6[1-35-7]|7[1-37-9]))\\d{7})|(840[013-9]\\d{6}|8402[19]\\d{5})"
ru_m = ru[:types][:mobile]
ru_m[:national_number_pattern] = "9([0-35-9]\\d{8}|4[1-9]\\d{7})"

of = File.new("./phone_data.dat", "wb")
of.write(Marshal.dump(d))
of.close
